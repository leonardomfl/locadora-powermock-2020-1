package br.ucsal.testequalidade20192.locadora;

import static org.powermock.api.mockito.PowerMockito.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocacaoBO.class, ClienteDAO.class, VeiculoDAO.class, LocacaoDAO.class })
public class LocacaoBOUnitarioTest {

	private Cliente obterDadosCliente() {
		return new Cliente("05146636508", "Leonardo Frenzel", "(71) 99340-7507");
	}

	private Modelo obterDadosModelo() {
		return new Modelo("Fusion");
	}

	private String obterDadosPlaca() {
		return "FKT-6842";
	}

	private Veiculo obterDadosVeiculo(String placa, Modelo modelo) {
		return new Veiculo(placa, 2018, modelo, 250.00);
	}

	private Date obterDadosData() {
		return new Date();
	}

	private List<String> criarRelacaoPlacas() {
		return new ArrayList<>();
	}

	private List<Veiculo> criarRelacaoVeiculos() {
		return new ArrayList<>();
	}

	private void adicionarPlacaRelacao(List<String> placas, String placa) {
		placas.add(placa);
	}

	private void adicionarVeiculoRelacao(List<Veiculo> veiculos, Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		// Instanciando os objetos que serão utilizados no teste.
		Cliente cliente = obterDadosCliente();
		Modelo modelo = obterDadosModelo();
		String placa = obterDadosPlaca();
		Veiculo veiculo = obterDadosVeiculo(placa, modelo);
		Date data = obterDadosData();
		List<String> placas = criarRelacaoPlacas();
		List<Veiculo> veiculos = criarRelacaoVeiculos();

		// Adicionando as placas e veículos em suas respectivas listas.
		adicionarPlacaRelacao(placas, placa);
		adicionarVeiculoRelacao(veiculos, veiculo);

		// Realizando a locação.
		Locacao locacao = new Locacao(cliente, veiculos, data, 3);

		// Utilizando o Mock.
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf(cliente.getCpf())).thenReturn(cliente);
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo);
		mockStatic(LocacaoDAO.class);
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		LocacaoBO.locarVeiculos(cliente.getCpf(), placas, data, 3);
		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf(cliente.getCpf());
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca(placa);
		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao);
		verifyNoMoreInteractions(LocacaoBO.class);
	}

}